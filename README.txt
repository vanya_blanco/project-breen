An application that allows to run an agent-based simulation of the public opinion game described in http://mpra.ub.uni-muenchen.de/45452/ .

The model depicts a public space where agents can have one of two possible opinions: 0 or 1. To run the program, double click on the jar file. The program takes as inputs from user:

-Number of agents in the simulation.
-Number of iterations the model is going to run.
-Random seed.
-How many agents hold opinion 1: here you set a proportion of agents thinking 1 is the right thing. Any decimal number between 0 and 1 is ok. 
-Initial belief: agents have a belief about how much supported is their opinion. They are bayesian beliefs, probabilities, so pick a number between 0 and 1. The closer to 1, the strongest an agent's belief that his opinion is the majoritarian one.
-hslash: a parameter that appears in the model, a constant. Write 0.369. If you choose some other value, probabilities won't be such, since output will be outside interval [0, 1].
-Payoff b: satisfaction you get if you find you agree with someone else. It can be any integer.
-Payoff c: satisfaction if find you disagree with someone else. It can be any integer smaller than previous b.